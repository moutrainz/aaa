(function () {
  const n = document.createElement("link").relList;
  if (n && n.supports && n.supports("modulepreload")) return;
  for (const e of document.querySelectorAll('link[rel="modulepreload"]')) s(e);
  new MutationObserver((e) => {
    for (const t of e)
      if (t.type === "childList")
        for (const i of t.addedNodes)
          i.tagName === "LINK" && i.rel === "modulepreload" && s(i);
  }).observe(document, { childList: !0, subtree: !0 });
  function r(e) {
    const t = {};
    return (
      e.integrity && (t.integrity = e.integrity),
      e.referrerPolicy && (t.referrerPolicy = e.referrerPolicy),
      e.crossOrigin === "use-credentials"
        ? (t.credentials = "include")
        : e.crossOrigin === "anonymous"
        ? (t.credentials = "omit")
        : (t.credentials = "same-origin"),
      t
    );
  }
  function s(e) {
    if (e.ep) return;
    e.ep = !0;
    const t = r(e);
    fetch(e.href, t);
  }
})();
const c = (o) => {
  let n = o.toDateString(),
    r = o.getHours(),
    s = o.getMinutes(),
    e = o.getSeconds(),
    t = r < 12 ? "AM" : "PM";
  return (
    (r = r < 10 ? (r = `0${r}`) : r),
    (s = s < 10 ? (s = `0${s}`) : s),
    (e = e < 10 ? (e = `0${e}`) : e),
    `
    <span class="display-time">
        <header><h3>${n}</h3></header>
        <p id="am-pm">${t}</p>
            <span class="box-container">
                <div class="box">
                    <p>${String(r)}</p>       
                </div>
                <p class="colon">:</p>
                <div class="box">
                    <p>${String(s)}</p>
                </div>
                <p class="colon">:</p>
                <div class="box">
                    <p>${String(e)}</p>
                </div>
            </span>
    </span>
    `
  );
};
const l = document.querySelector("#app"),
  a = 1e3;
setInterval(() => {
  l.innerHTML = `
    <section class="container">
      ${c(new Date)}
    </section>
  `;
}, a);

// ...

// Add the "Made With ❤️ By Abdullah Azhary" below the app
const madeWithLove = document.createElement("div");
madeWithLove.innerHTML = '<p>Made With ❤️ By Abdullah Azhary</p>';
madeWithLove.style.color = 'white';  // Set the text color to white
madeWithLove.style.position = 'absolute';  // Position it absolutely
madeWithLove.style.bottom = '40px';  // Adjust the distance from the bottom
madeWithLove.style.left = '50%';  // Center horizontally
madeWithLove.style.transform = 'translateX(-50%)';  // Adjust for centering
madeWithLove.style.fontSize = '14px';  // Adjust the font size
madeWithLove.style.textAlign = 'center';  // Center the text
document.body.appendChild(madeWithLove);
 
